﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class nameInput : MonoBehaviour
{
    public InputField textBox;
    public Button continueBtn;
    
    public HighScoreTable highScoreTable;
    public GameObject winningScreen;
    public GameObject highScoreObj;
    public Text timerText;
    public void clickSaveButton()
    {
        if (textBox.text == null)
        {
            PlayerPrefs.SetString("playerName", "Anonymous");
        }
        else 
        {
            PlayerPrefs.SetString("playerName", textBox.text);
        }
        highScoreTable.AddHighscoreEntry(PlayerPrefs.GetInt("coins"),timerText.text,PlayerPrefs.GetInt("playerScore"),PlayerPrefs.GetString("playerName"));
        textBox.enabled = false;
        continueBtn.enabled = false;
        
        highScoreObj.SetActive(true);
        winningScreen.SetActive(false);
        
        Debug.Log("I was called sadly");
    }

}
