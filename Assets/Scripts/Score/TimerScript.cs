﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerScript : MonoBehaviour
{
    public Text timerText;
   // public Text BonusScore;
    public static float startTime;
    private bool finnished = false;

    

    

    void Start()
    {
        startTime = Time.timeSinceLevelLoad;
    }
    void Update()
    {
        
        if (finnished) return;  
    
        float t = Time.timeSinceLevelLoad - startTime - 15.75f;
        float LevelTime = 600 - t;
        int LevelTimeInt = (int)LevelTime;
        string BonusTimeScore = (LevelTimeInt * 10).ToString();

        string minutes = ((int)t / 60).ToString();
        string seconds = (t % 60).ToString("f0");
        string milliseconds = ((t * 100) % 100).ToString("f0");

        if (t >= 0)
        {
            
            switch (minutes)
            {
                default: minutes.ToString(); break;

                case "0": minutes = "00"; break;
                case "1": minutes = "01"; break;
                case "2": minutes = "02"; break;
                case "3": minutes = "03"; break;
                case "4": minutes = "04"; break;
                case "5": minutes = "05"; break;
                case "6": minutes = "06"; break;
                case "7": minutes = "07"; break;
                case "8": minutes = "08"; break;
                case "9": minutes = "09"; break;
            }

            
            switch (seconds)
            {
                default: seconds.ToString(); break;

                case "0": seconds = "00"; break;
                case "1": seconds = "01"; break;
                case "2": seconds = "02"; break;
                case "3": seconds = "03"; break;
                case "4": seconds = "04"; break;
                case "5": seconds = "05"; break;
                case "6": seconds = "06"; break;
                case "7": seconds = "07"; break;
                case "8": seconds = "08"; break;
                case "9": seconds = "09"; break;
                case "60": seconds = "00"; break;
            }


            
            switch (milliseconds)
            {
                default: milliseconds.ToString(); break;

                case "0": milliseconds = "00"; break;
                case "1": milliseconds = "01"; break;
                case "2": milliseconds = "02"; break;
                case "3": milliseconds = "03"; break;
                case "4": milliseconds = "04"; break;
                case "5": milliseconds = "05"; break;
                case "6": milliseconds = "06"; break;
                case "7": milliseconds = "07"; break;
                case "8": milliseconds = "08"; break;
                case "9": milliseconds = "09"; break;
            }
        }
        else
        {
            minutes = "00";
            seconds = "00";
            milliseconds = "00";
        }


        timerText.text = minutes + ":" + seconds + ":" + milliseconds;
        //BonusScore.text ="+" + BonusTimeScore;
    }

    public void Finnish()
    {
        finnished = true;
        timerText.color = Color.yellow;
    }
}
