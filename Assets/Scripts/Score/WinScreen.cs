﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WinScreen : MonoBehaviour
{
    public Text FinalScoreUI;
    public GameObject WinScreenUI;
    public GameObject HighScoreTable;

    int LevelTimeInt = 0;
    int BonusTimeScore = 0;

    public PlayerMovement playerMovement;

    public void LoadMenu()
    {
        HighScoreTable.SetActive(false);
        ScoreScript.gems = 0;
        ExitScript.won = false;
        PauseMenu.GameIsPaused = false;
        ScoreScript.exitCheckLeft = false;
        ScoreScript.exitCheckMid = false;
        ScoreScript.exitCheckRight = false;
        Health.health = 5;
        ScoreScript.score = 0;

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
        Time.timeScale = 1f;
        WinScreenUI.SetActive(false);
        SceneManager.LoadScene("MainMenu");
    }
    public void ResetLevel()
    {
        

        Cursor.lockState = CursorLockMode.Locked;
        ScoreScript.gems = 0;
        PauseMenu.GameIsPaused = false;
        ScoreScript.exitCheckLeft = false;
        ScoreScript.exitCheckMid = false;
        ScoreScript.exitCheckRight = false;
        Health.health = 5;
        ScoreScript.score = 0;
        

        ExitScript.won = false;
        Cursor.visible = false;
        Time.timeScale = 1f;
        WinScreenUI.SetActive(false);
        SceneManager.LoadScene("Arena1");

    }
    bool winUIisActivated = false;
    public void Update()
    {
        if (ExitScript.won == true)
        {
            if (winUIisActivated == false)
            {
                WinningScreen();
                winUIisActivated = true;
            }
        }
    }
    void WinningScreen()
    {
        playerMovement.stopMusic();
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;

        PauseMenu.GameIsPaused = true;

        WinScreenUI.SetActive(true);

        float t = Time.time - TimerScript.startTime;
        string tString = t.ToString();
        float LevelTime = 600 - t;
        LevelTimeInt = (int)LevelTime;
        BonusTimeScore = (LevelTimeInt * 100);
        if (BonusTimeScore <= 0) BonusTimeScore = 0;

        int FinalScore = ScoreScript.score + (ScoreScript.gems * 100) + BonusTimeScore;

        FinalScoreUI.text = "your Score: " + FinalScore;

        PlayerPrefs.SetInt("playerScore", FinalScore);

        Time.timeScale = 0f;
       // Debug.Log("Ýou Won");
        
    }
}
