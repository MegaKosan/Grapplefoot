﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, 90 * Time.deltaTime , 0);
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.name == "Player")
        {
            if (gameObject.tag == "coin_yellow")
            {
                other.GetComponent<ScoreScript>().points += 10;
                Destroy(gameObject);
            }
            else if(gameObject.tag == "coin_red")
            {
                other.GetComponent<ScoreScript>().points += 50;
                Destroy(gameObject);
            }
            else if(gameObject.tag == "coin_blue")
            {
                other.GetComponent<ScoreScript>().points += 250;
                Destroy(gameObject);
            }
        }
    }
}
