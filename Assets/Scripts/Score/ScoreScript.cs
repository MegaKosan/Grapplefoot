﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour
{
    public Text scoreText;

    public int points = 0;
    public static int score = 0;
    public static int gems = 0;
    public int gemstotal = 3;

    public GameObject gemLeft, gemMid, gemRight, levelExit;
    public GameObject checkLeft, checkMid, checkRight;
    public Material red, green;

    public static bool exitCheckLeft, exitCheckMid, exitCheckRight = false;

    private void Update()
    {
        gemCheck();
        

        if (points == 0) score = 0;
        while (points > score)
        {
            score += 1;
        }
        PlayerPrefs.SetInt("coins",points);
        //PlayerPrefs.SetInt("playerScore", score);
        scoreText.text = score.ToString();
        
    }

    private void gemCheck()
    {

        
        if (gemLeft.GetComponent<GemScript>().gemDestroyed)
        {
            checkLeft.SetActive(true);
            //checkLeft.GetComponent<Renderer>().material = green;
            exitCheckLeft = true;
        }
        if (gemMid.GetComponent<GemScript>().gemDestroyed)
        {
            checkMid.SetActive(true);
            //checkMid.GetComponent<Renderer>().material = green;
            exitCheckMid = true;
            
        }
        if (gemRight.GetComponent<GemScript>().gemDestroyed)
        {
            checkRight.SetActive(true);
            //checkRight.GetComponent<Renderer>().material = green;
            exitCheckRight = true;
        }
        if (!gemLeft.GetComponent<GemScript>().gemDestroyed)
        {
        checkLeft.SetActive(false);
        
            //checkLeft.GetComponent<Renderer>().material = red;
        }
        if (!gemMid.GetComponent<GemScript>().gemDestroyed)
        {
        checkMid.SetActive(false);
        
            //checkMid.GetComponent<Renderer>().material = red;
        }
        if (!gemRight.GetComponent<GemScript>().gemDestroyed)
        {
        
        checkRight.SetActive(false);
            //checkRight.GetComponent<Renderer>().material = red;
        }

        if (gemRight.GetComponent<GemScript>().gemDestroyed && gemMid.GetComponent<GemScript>().gemDestroyed && gemLeft.GetComponent<GemScript>().gemDestroyed)
        {
            levelExit.SetActive(true);
        }
        else levelExit.SetActive(false);
    }
    /*private void OnGUI()
       {


           guiStyle.normal.textColor = Color.white;
           guiStyle.fontSize = 60;
           GUI.Label(new Rect(10, 100, 100, 20), "Gems : " + gems.ToString() +" / 3", guiStyle);

       }*/


}
