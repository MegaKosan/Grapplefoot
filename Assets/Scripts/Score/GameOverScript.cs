﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverScript : MonoBehaviour
{
    public GameObject GameOverUI;
    public Text FinalScoreUI;
    public GameObject HighScoreTable;

    public PlayerMovement playerMovement;

    private void Update()
    {
        if (GameOver.isGameOver == true)
        {
            
            GameOverScreen();
        }
    }
    void GameOverScreen()
    {
        playerMovement.stopMusic();
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;

        PauseMenu.GameIsPaused = true;

        int FinalScore = ScoreScript.score + (ScoreScript.gems * 100);
        Time.timeScale = 0f;
        Debug.Log("Game Over");
        GameOverUI.SetActive(true);
        FinalScoreUI.text = "your Score: " + FinalScore;
    }
    public void LoadMenu()
    {
        HighScoreTable.SetActive(false);
        PauseMenu.GameIsPaused = false;
        ScoreScript.gems = 0;
        ScoreScript.exitCheckLeft = false;
        ScoreScript.exitCheckMid = false;
        ScoreScript.exitCheckRight = false;
        Health.health = 5;
        ScoreScript.score = 0;
        TimerScript.startTime = 0;

        GameOver.isGameOver = false;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
        Time.timeScale = 1f;
        GameOverUI.SetActive(false);
        SceneManager.LoadScene("MainMenu");
    }
    public void ResetLevel()
    {
        PauseMenu.GameIsPaused = false;
        ScoreScript.gems = 0;
        GameOver.isGameOver = false;
        ScoreScript.exitCheckLeft = false;
        ScoreScript.exitCheckMid = false;
        ScoreScript.exitCheckRight = false;
        Health.health = 5;
        ScoreScript.score = 0;
        TimerScript.startTime = 0;

        Cursor.lockState = CursorLockMode.Locked;
        Time.timeScale = 1f;
        Debug.Log("Restarting ...");
        GameOverUI.SetActive(false);
        SceneManager.LoadScene("Arena1");
    }
}
