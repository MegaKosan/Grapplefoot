﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScoreTable : MonoBehaviour
{
    public Transform entryContainer;
    public Transform entryTemplate;
    private List<HighScoreEntry> highscoreEntryList;
    private List<Transform> highscoreEntryTransformList;

    private int playerHighScore = ScoreScript.score;

    string ListSorting = "NewList";

    private void Start()
    {
        if (entryContainer == null) entryContainer = transform.parent.Find("highscoreEntryContainer");
        if (entryTemplate == null) entryTemplate = transform.Find("highscoreEntryTemplate");
    }
    private void Awake()
    {
       
        if (entryTemplate != null) entryTemplate.gameObject.SetActive(false);

        string jsonString = PlayerPrefs.GetString("TableScore");
        Highscores highscores = JsonUtility.FromJson<Highscores>(jsonString);
        if (highscores == null)
        {
            // There's no stored table,make a new table
            Debug.Log("Initializing table with default values...");
            AddHighscoreEntry(2200, "2:00:15",50100, "Moudin");
            AddHighscoreEntry(2470, "13:23:04",2470 ,"Gimble_Chedalf");
            AddHighscoreEntry(800, "2:46:67",43880,"Roy");
            AddHighscoreEntry(2920, "01:59:39",50920, "Kosan");
            AddHighscoreEntry(2920, "01:40:39", 51000, "Grapplefoot");
            AddHighscoreEntry(390, "1:43:91",48390 ,"Alex");
            AddHighscoreEntry(400, "1:51:43", 47600,"JellyJelly");
            AddHighscoreEntry(1970, "4:55:02", 29170, "Broccoli");
            AddHighscoreEntry(5060, "18:27:58", 5060, "SIMUTECH_Intern");
            // Reload
            jsonString = PlayerPrefs.GetString("TableScore");
            highscores = JsonUtility.FromJson<Highscores>(jsonString);
        }
        //AddHighscoreEntry(1823, "2:18:82", 999200, "DeadUglyNoob");//Test Entry

        // Bubble Sort entry list by Score
        for (int i = 0; i < highscores.highscoreEntryList.Count; i++)
        {
            for (int j = i + 1; j < highscores.highscoreEntryList.Count; j++)
            {
                if (highscores.highscoreEntryList[j].score > highscores.highscoreEntryList[i].score)
                {
                    
                    HighScoreEntry tmp = highscores.highscoreEntryList[i];
                    highscores.highscoreEntryList[i] = highscores.highscoreEntryList[j];
                    highscores.highscoreEntryList[j] = tmp;
                }
            }
        }

        highscoreEntryTransformList = new List<Transform>();
        if (highscores.highscoreEntryList.Count < 10)
        {
            foreach (HighScoreEntry highScoreEntry in highscores.highscoreEntryList)
            {
                CreateHighScoreEntryTransform(highScoreEntry, entryContainer, highscoreEntryTransformList);
            }
        }
        else if (highscores.highscoreEntryList.Count >= 10) 
        {
            for (int i = 0; i < 10; i++) 
            {
                CreateHighScoreEntryTransform(highscores.highscoreEntryList[i], entryContainer, highscoreEntryTransformList);
            }
        }
        
    }
    public void AddHighscoreEntry(int coins, string time, int score, string name)
    {
        HighScoreEntry highScoreEntry = new HighScoreEntry { coins = coins, time = time, score = score, name = name };

        string jsonString = PlayerPrefs.GetString("TableScore");
        Highscores highscores = JsonUtility.FromJson<Highscores>(jsonString);
       
        if (highscores == null)
        {
           
            highscores = new Highscores()
            {
                highscoreEntryList = new List<HighScoreEntry>()
            };
        }

        highscores.highscoreEntryList.Add(highScoreEntry);

        string json = JsonUtility.ToJson(highscores);
        PlayerPrefs.SetString("TableScore", json);
        PlayerPrefs.Save();
    }
    private void CreateHighScoreEntryTransform(HighScoreEntry highscoreEntry, Transform container, List<Transform> transformList)
    {


        float templateHeight = 20f;

            Transform entryTransform = Instantiate(entryTemplate, container);
            RectTransform entryRectTransform = entryTransform.GetComponent<RectTransform>();
            entryRectTransform.anchoredPosition = new Vector2(0, -templateHeight * transformList.Count);
            entryTransform.gameObject.SetActive(true);

            int rank = transformList.Count + 1;
            string rankString;
            switch (rank)
            {
                default:
                    rankString = rank + "TH"; break;

                case 1: rankString = "1ST"; break;
                case 2: rankString = "2ND"; break;
                case 3: rankString = "3RD"; break;
            }

            entryTransform.Find("posText").GetComponent<Text>().text = rankString;

        int coins = highscoreEntry.coins;
        entryTransform.Find("coinText").GetComponent<Text>().text = coins.ToString();

        string time = highscoreEntry.time;
        entryTransform.Find("timeText").GetComponent<Text>().text = time.ToString();

        int score = highscoreEntry.score;
        entryTransform.Find("scoreText").GetComponent<Text>().text = score.ToString();

        string name = highscoreEntry.name;
        entryTransform.Find("nameText").GetComponent<Text>().text = name;

            transformList.Add(entryTransform);
        
    }

    public void SortByCoins()
    {
        ListSorting = "Coins";
        for (int i = 0; i < highscoreEntryList.Count; i++)
        {
            for (int j = i + 1; j < highscoreEntryList.Count; j++)
            {
                if (highscoreEntryList[j].coins > highscoreEntryList[i].coins)
                {
                    HighScoreEntry tmp = highscoreEntryList[i];
                    highscoreEntryList[i] = highscoreEntryList[j];
                    highscoreEntryList[j] = tmp;
                }
            }
        }
    }
    public void SortByTime()
    {
        ListSorting = "Time";
        for (int i = 0; i < highscoreEntryList.Count; i++)
        {
            for (int j = i + 1; j < highscoreEntryList.Count; j++)
            {
               
                if (GetTimeInMilliSec( highscoreEntryList[j].time) > GetTimeInMilliSec( highscoreEntryList[i].time))
                {
                    HighScoreEntry tmp = highscoreEntryList[i];
                    highscoreEntryList[i] = highscoreEntryList[j];
                    highscoreEntryList[j] = tmp;
                }
            }
        }
    }
    public void SortByScore()
    {
        ListSorting = "Score";
        for (int i = 0; i < highscoreEntryList.Count; i++)
        {
            for (int j = i + 1; j < highscoreEntryList.Count; j++)
            {
                if (highscoreEntryList[j].score > highscoreEntryList[i].score)
                {
                    HighScoreEntry tmp = highscoreEntryList[i];
                    highscoreEntryList[i] = highscoreEntryList[j];
                    highscoreEntryList[j] = tmp;
                }
            }
        }
    }
    private int GetTimeInMilliSec(string strTime) 
    {
        string minutes=strTime.Substring(0,strTime.IndexOf(":"));
        string seconds=strTime.Split(':', ':')[1];// [1] means it selects second part of your what you split parts of your string. (Zero based)
        string milliseconds =strTime.Substring(strTime.LastIndexOf(':')+1);


        int t = (int.Parse(minutes) * 60000) + (int.Parse(seconds) * 1000) + int.Parse(milliseconds);
        return t;
    }
    public void HighScoreBack()
    {
        PauseMenu.HighScoreEnabled = false;
    }
    private class Highscores
    {
        public List<HighScoreEntry> highscoreEntryList;

    }

    [System.Serializable]
    private class HighScoreEntry {
        public int coins;
        public string time;
        public int score;
        public string name;

    }

}
