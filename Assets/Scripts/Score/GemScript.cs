﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemScript : MonoBehaviour
{
    public bool gemDestroyed = false;
    public GameObject gemBeam; 
    public GameObject gemBody;
    public GameObject gemParticles
    ;

    private void Start()
    {
        gemBeam.SetActive(false);
    }

    public void OnKick()
    {
        gemBeam.SetActive(true);
        gemDestroyed = true;
        gemBody.SetActive(false);
        gemParticles.SetActive(false);
    }
}
