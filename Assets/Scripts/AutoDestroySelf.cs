﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestroySelf : MonoBehaviour
{
    [Tooltip("Amount of lifetime before this object automatically destroys itself.")]
    public float lifetime = 10f;

    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, lifetime);
    }
}
