﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrapplingHook : MonoBehaviour
{
    public GameObject Hook;
    public GameObject HookHolder;

    public float hooktravelspeed;
    public float playertravelspeed;

    public bool hooked;
    public static bool fired;
    public GameObject hookedObj;

    public float maxDistance;
    private float currentDistance;

    public bool grounded;

    private void Update()
    {
        if(Input.GetMouseButtonDown(0) && fired == false)
            fired = true;

        if (fired == true && hooked == false)
        {
            Hook.transform.Translate(Vector3.forward * Time.deltaTime * hooktravelspeed);
            currentDistance = Vector3.Distance(transform.position, Hook.transform.position);

            if (currentDistance >= maxDistance)
                ReturnHook();

        }

        if (hooked == true && fired == true)
        {
            Hook.transform.parent = hookedObj.transform;
            transform.position = Vector3.MoveTowards(transform.position, Hook.transform.position, Time.deltaTime * playertravelspeed);
            float distanceToHook = Vector3.Distance(transform.position, Hook.transform.position);

            this.GetComponent<Rigidbody>().useGravity = false;

            if (distanceToHook < 1)
            {
                if(grounded == false)
                {
                    this.transform.Translate(Vector3.forward * Time.deltaTime * 10f);
                    this.transform.Translate(Vector3.up * Time.deltaTime * 15f);
                }

                StartCoroutine("Climb");

            }
        }
        else
        {
            Hook.transform.parent = HookHolder.transform;
            this.GetComponent<Rigidbody>().useGravity = true;
        }
    }

    IEnumerator Climb()
    {
        yield return new WaitForSeconds(0.1f);
        ReturnHook();
    }

    void ReturnHook()
    {
        Hook.transform.rotation = HookHolder.transform.rotation;
        Hook.transform.position = HookHolder.transform.position;
        fired = false;
        hooked = false;
    }

    void CheckIfGrounded()
    {
        RaycastHit hit;
        float distance = 1f;

        Vector3 dir = new Vector3(0, -1);

        if(Physics.Raycast(transform.position, dir, out hit, distance))
        {
            grounded = true;
        }
        else
        {
            grounded = false;
        }
    }
}
