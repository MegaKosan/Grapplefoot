using System.Collections;
using UnityEngine;


public class GrapplingGun : MonoBehaviour {

    public LineRenderer lr;
    public Camera playerCam;
    private Vector3 grapplePoint;
    public LayerMask whatIsGrappleable;
    public Transform gunTip, head, player;
    private float maxDistance = 25f;
    private SpringJoint joint;
    public bool grappleState = false;

    public float SpherecastRadius = 1;
    public GameObject currentHitObject;
    private float currentHitDistance;

    public bool grappled = false;

    public GameObject HookProjectile;
    private bool ropeFlag;
    private bool emptyShot;
    private bool gunReady;
    public float fireInterval;
    public float hooktravelspeed=20f, playerPushForce;

    public float grappleSpring = 5f, grappleDamper = 10f, grappleMassScale = 3f, minActiveDistance = 4f, distanceCheckDelay = .7f;

    public GameObject hookedObj;

    private Animator anim;

  
    private Spring spring;
    [Header("Grapple Animation")]
    public int quality;
    public float damper;
    public float strength;
    public float velocity;
    public float waveCount;
    public float waveHeight;
    public AnimationCurve affectCurve;

    //Effects
    [Header("Effects")]
    public GameObject dustPuffParticle;
    public GameObject impactDecal;

    //Sound Sources
    [Header("Audio Sources")]
    public AudioSource audioGrappleShoot;
    public AudioSource audioGrappleHookImpact;
    public AudioSource audioGrappleRetract;
    public AudioSource audioGrappleHookTravel;
    public AudioSource audioGrappling;

    void Awake() {
        anim = player.GetComponent<Animator>();
        spring = new Spring();
        spring.SetTarget(0);
        gunReady = true;
    }

    void Update() {
        if (Input.GetButtonDown("Grapple") && grappled == false && PauseMenu.GameIsPaused == false && ExitScript.won == false)
        {
            if (gunReady)
            {
                StartGrapple();              
            }
        }
        else if (Input.GetButtonDown("Grapple") && grappled == true && PauseMenu.GameIsPaused == false)
        {
            StopGrapple();
        }

        if (grappled)
        {
            WhilstGrappling();
            ShootProjectile();
        }
        
    }

    //Called after Update
    void LateUpdate() {
        DrawRope(ropeFlag);
        DrawEmptyShoot();
    }

    /// <summary>
    /// Call whenever we want to start a grapple
    /// </summary>
    void StartGrapple() {
        audioGrappleShoot.PlayOneShot(audioGrappleShoot.clip);
        anim.SetTrigger("GrappleShoot");
        gunReady = false;
        RaycastHit hit;
        
      if (Physics.SphereCast(playerCam.transform.position, GetRadius(GetDistanceToRaycast()), playerCam.transform.forward, out hit, maxDistance, whatIsGrappleable, QueryTriggerInteraction.UseGlobal)) {
            currentHitObject = hit.transform.gameObject;
            currentHitDistance = hit.distance;

            grappled = true;


            //Move hit sound to hit point and play
            audioGrappleHookImpact.gameObject.transform.position = grapplePoint;
            audioGrappleHookImpact.PlayOneShot(audioGrappleHookImpact.clip);

            //Optional: Add slight pitch/volume shift based on grapple length or movement speed, probably in method WhilstGrappling()
            audioGrappling.Play();

            anim.SetBool("Grappling", true);

            grapplePoint = hit.point;
            joint = player.gameObject.AddComponent<SpringJoint>();
            joint.autoConfigureConnectedAnchor = false;
            joint.connectedAnchor = grapplePoint;

            float distanceFromPoint = Vector3.Distance(player.position, grapplePoint);


            //The distance grapple will try to keep from grapple point. 

            joint.maxDistance = distanceFromPoint * 0.0001f;
            joint.minDistance = distanceFromPoint * 0.0001f;

            // Adjustable in player prefab
            joint.spring = grappleSpring;
            joint.damper = grappleDamper;
            joint.massScale = grappleMassScale;

        }
        else
        {
            currentHitDistance = maxDistance;
            currentHitObject = null;
            StartCoroutine(ShootEmpty(.28f));
            StartCoroutine("ResetGunReady");

        }
       
    }


    /// <summary>
    /// Call whenever we want to stop a grapple
    /// </summary>
    public void StopGrapple() {
        if (grappled)
        {
            grappled = false;
            ropeFlag = false;
            StopCoroutine("DistanceCheck");
            ReturnProjectile();
            audioGrappling.Stop();
            audioGrappleRetract.PlayOneShot(audioGrappleRetract.clip);

            anim.SetBool("Grappling", false);


            lr.positionCount = 0;
            Destroy(joint);
            gunReady = true;
        }
    }
    float GetDistanceToRaycast() 
    {
        RaycastHit hitInfo;
        if (Physics.SphereCast(playerCam.ScreenPointToRay(Input.mousePosition),1f, out hitInfo, maxDistance, whatIsGrappleable, QueryTriggerInteraction.UseGlobal))
        {
            return Vector3.Distance(hitInfo.point,playerCam.transform.position);
        }
        else 
        {
            return maxDistance;
        }
    }
    float GetRadius(float distance) 
    {
        if (distance >= (maxDistance-6)) return SpherecastRadius;
        else if (distance > 1.5f && distance < (maxDistance-6)) return SpherecastRadius * (distance / (maxDistance-6));
        else if (distance < 1.5f) return 0.01f;
        else return 0.01f;  
    }
    public bool IsGrappling()
    {
        return joint != null;
    }
    public Vector3 GetGrapplePoint()
    {
        return grapplePoint;
    }

    private Vector3 currentGrapplePosition;

    void DrawRope(bool flag) {

        //If not grappling, don't draw rope
        if (!joint && !emptyShot) 
        {
            currentGrapplePosition = gunTip.position;
            spring.Reset();
            if (lr.positionCount > 0)
                lr.positionCount = 0;
            return;
        }

        if (lr.positionCount == 0)
        {
            spring.SetVelocity(velocity);
            lr.positionCount = quality + 1;
        }

        spring.SetDamper(damper);
        spring.SetStrength(strength);
        spring.UpdateSpring(Time.deltaTime);

        
        
        var up = Quaternion.LookRotation((grapplePoint - gunTip.position).normalized) * Vector3.up;

        if (ropeFlag)
        {
            currentGrapplePosition = Vector3.Lerp(currentGrapplePosition, grapplePoint, Time.deltaTime * 12f);
        }
        else 
        {
            currentGrapplePosition = Vector3.Lerp(currentGrapplePosition, HookProjectile.transform.position, Time.deltaTime * 12f);
        }

        for (var i = 0; i < quality + 1; i++)
        {
            var delta = i / (float)quality;
            var offset = up * waveHeight * Mathf.Sin(delta * waveCount * Mathf.PI) * spring.Value * affectCurve.Evaluate(delta);

            lr.SetPosition(i, Vector3.Lerp(gunTip.position, currentGrapplePosition, delta) + offset);
        }
    }
    void DrawEmptyShoot()
    {

        //If not grappling, don't draw rope
        if (!emptyShot)
        {
             return;
        }
        else
        {
            if (lr.positionCount == 0)
            {
                spring.SetVelocity(velocity);
                lr.positionCount = quality + 1;
            }

            spring.SetDamper(0f);
            spring.SetStrength(strength/20);
            spring.UpdateSpring(Time.deltaTime);

            var up = Quaternion.LookRotation((gunTip.position-grapplePoint).normalized) * Vector3.up;

            currentGrapplePosition = Vector3.Lerp(currentGrapplePosition, grapplePoint, Time.deltaTime * 12f);
            
            for (var i = 0; i < quality + 1; i++)
            {
                var delta = i / (float)quality;
                var offset = up * (waveHeight) * Mathf.Sin(delta * (waveCount) * Mathf.PI) * spring.Value * affectCurve.Evaluate(delta);

                lr.SetPosition(i, Vector3.Lerp(gunTip.position, currentGrapplePosition, delta) + offset);
            }
        }
    }
    public void WhilstGrappling()
    {
        //Push player towards target
        Vector3 pushDirection = grapplePoint - player.position;
        pushDirection.Normalize();
        player.GetComponent<Rigidbody>().AddForce(pushDirection * Time.deltaTime * playerPushForce);
        StartCoroutine("DistanceCheck");

    }
    IEnumerator DistanceCheck()
    {
        yield return new WaitForSeconds(distanceCheckDelay);
        float distance = Vector3.Distance(grapplePoint, this.transform.position);
        if (distance < minActiveDistance) StopGrapple(); StopCoroutine("DistanceCheck");
    }

    void ShootProjectile()
    {
        if (grappled)
        {
            if (!ropeFlag)
            {
                HookProjectile.SetActive(true);
                float distance = Vector3.Distance(grapplePoint, HookProjectile.transform.position);
                Vector3 direction = grapplePoint - HookProjectile.transform.position;
                if (distance > 0f)
                {
                    HookProjectile.transform.position += direction * Time.deltaTime * hooktravelspeed;
                   
                    if (distance <= 1f) 
                    {
                        ropeFlag = true;

                        Instantiate(impactDecal, grapplePoint, gunTip.transform.rotation);
                        Instantiate(dustPuffParticle, grapplePoint, gunTip.transform.rotation);
                    }
                }
            }
            else
            {
                HookProjectile.transform.position = grapplePoint;

            }
        }
    }
    void ReturnProjectile()
    {
        ResetProjectilePosition();
        HookProjectile.SetActive(false);

    }

    void ResetProjectilePosition() 
    {
        HookProjectile.transform.position = gunTip.transform.position;
        HookProjectile.transform.rotation = gunTip.transform.rotation;
    }

    IEnumerator ShootEmpty(float shootTime) 
    {
        emptyShot = true;
        gunReady = false;
        grapplePoint= HookProjectile.transform.position+ HookProjectile.transform.forward*maxDistance;
        HookProjectile.SetActive(true);
        HookProjectile.transform.position = grapplePoint;

        yield return new WaitForSeconds(shootTime);
        HookProjectile.transform.position = Vector3.Lerp(HookProjectile.transform.position, gunTip.position, Time.deltaTime * 12);
        ReturnProjectile();
        gunReady = true;
        emptyShot = false;
    }

    IEnumerator ResetGunReady() 
    {
        yield return new WaitForSeconds(fireInterval);
        gunReady = true;
    }
}
