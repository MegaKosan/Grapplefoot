﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookDetector : MonoBehaviour
{
    public GameObject player;
    private void OnTriggerEnter(Collider other)
    {
        //maybe need to change tags of objects later on
        if (other.tag == "Untagged")
        {
            player.GetComponent<GrapplingHook>().hooked = true;
            player.GetComponent<GrapplingHook>().hooked = other.gameObject;

        }
    }
}
