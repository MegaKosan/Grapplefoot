﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetScript : MonoBehaviour
{


    public GameObject target;
    public Transform playerHead;

    private GameObject player; 
    private Rigidbody playerRb;

    public float closeDistance = 7.5f;
    public float pushStrength = 10f;

    public float rotationSpeed = 2.0f;
    private bool runIntialCode = true;

    private void Start()
    {
        player = playerHead.parent.gameObject;
        playerRb = player.GetComponent<Rigidbody>();
    }

    void Update()
    {
        var target = EnemyTargeting.FindClosestEnemy(transform.position);
        
        if (target)
        {
            Quaternion initialHeadRot = playerHead.transform.rotation;

            Vector3 offset = target.transform.position - playerHead.transform.position;
            float sqrLen = offset.sqrMagnitude;

            Vector3 toTarget = (target.transform.position - playerHead.transform.position).normalized;

            //Check if enemy is in LOS
            Ray ray = new Ray(playerHead.position, toTarget);
            RaycastHit hit;
            Physics.Raycast(ray, out hit, closeDistance);

            //Check if enemy is within distance and los
            if (sqrLen < closeDistance * closeDistance && hit.collider.tag == "Enemy")
            {


                print("The Enemy is close to me!");
                if (Input.GetButton("Kick") && Vector3.Dot(toTarget, playerHead.transform.forward) > 0)
                {
                    //Rotate Playerhead
                    Vector3 targetPosition = new Vector3(target.transform.position.x, target.transform.position.y, target.transform.position.z);
                    //playerHead.transform.rotation = Quaternion.Slerp(playerHead.transform.rotation, (Quaternion.LookRotation(toTarget)), Time.deltaTime * rotationSpeed);
                    playerHead.transform.rotation = Quaternion.Slerp(initialHeadRot, (Quaternion.LookRotation(toTarget)), Time.deltaTime * rotationSpeed);

                    //Push player towards target
                    if (runIntialCode) playerRb.velocity = new Vector3(0f, 1f, 0f);
                    playerRb.AddForce(toTarget * pushStrength * Time.deltaTime);

                    runIntialCode = false;
                }
                else runIntialCode = true;

            }
            
        }
        else
        {
            runIntialCode = true;
        }
    }
}

