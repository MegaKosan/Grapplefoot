

using System;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    //Assingables
    [Header("Assignables")]
    public Transform playerCam;
    public Transform orientation;
    public Transform playerBody;
    public GameObject grappleGun;
    
    //Other
    private Rigidbody rb;
    private Animator anim;

    //Rotation and look
    private float xRotation;
    private float sensitivity = 50f;
    private float sensMultiplier = 1f;

    //Movement
    [Header("Movement")]
    public float extraGravity = 50f;
    public float moveForce = 2000;
    public float moveSpeed = 10;
    public float aerialMovementMultiplier = 0.5f;
    public bool grounded;
    public LayerMask whatIsGround;
    
    public float counterMovement = 0f;
    private float threshold = 0f;
    public float maxSlopeAngle = 35f;



    //Jumping
    private bool readyToJump = true;
    private float jumpCooldown = 0.25f;
    public float jumpForce = 550f;
    public float wallJumpUpwardForce = 600f;
    public float wallJumpSideForce = 1000f;

    //Input
    float x, y;
    bool jumping, sprinting;

    //Sliding - itself deprecated, but variables are still used though
    private Vector3 normalVector = Vector3.up;
    private Vector3 wallNormalVector;

    //wall-run attributes
    [Header("Wallrunning")]
    [SerializeField]
    public LayerMask whatIsWall;
    public Transform wallCheckLeft;
    public Transform wallCheckRight;
    public float wallrunForce,wallRunExitForce=150, maxWallrunTime = 3.5f, timeLeft, maxWallSpeed;
    private bool isWallRight, isWallLeft,isWallTimerOn;
    private bool isWallRunning;
    public float maxWallRunCameraTilt, wallRunCameraTilt, cameraRotationSpeed=4;

    //FMod Sounds
    [Header("FMOD Sounds")]
    [FMODUnity.EventRef]
    public string FootstepsFloorStoneEvent = "";
    [FMODUnity.EventRef]
    public string FootstepsWallEvent = "";
    [FMODUnity.EventRef]
    public string FootstepsGrassEvent = "";


    [FMODUnity.EventRef]
    public string PlayerJumpEvent = "";
    [FMODUnity.EventRef]
    public string PlayerLandEvent = "";
    [FMODUnity.EventRef]
    public string PlayerHitEvent = "";
    [FMODUnity.EventRef]
    public string PlayerFlyEvent = "";
    FMOD.Studio.EventInstance flyState;

    [FMODUnity.EventRef]
    public string bootUpSound = "";

    [FMODUnity.EventRef]
    public string musicEvent = "";
    FMOD.Studio.EventInstance musicState;


    void Awake() {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        
    }
    
    void Start() {
        musicState = FMODUnity.RuntimeManager.CreateInstance(musicEvent);

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        anim.SetTrigger("eff_Spawning");
        FMODUnity.RuntimeManager.PlayOneShot(bootUpSound);
        PauseMenu.GameIsPaused = false;
        ScoreScript.gems = 0;
        ScoreScript.exitCheckLeft = false;
        ScoreScript.exitCheckMid = false;
        ScoreScript.exitCheckRight = false;
        Health.health = 5;
        ScoreScript.score = 0;
      
        GameOver.isGameOver = false;
    }

    
    private void FixedUpdate() {
        
    }

    private void Update() {
        MyInput();
        Look();
        CheckForWall();
        WallRunInput();
        Movement();
    }

    /// <summary>
    /// Find user input. Should put this in its own class but im lazy
    /// </summary>
    private void MyInput() {
        x = Input.GetAxisRaw("Horizontal");
        y = Input.GetAxisRaw("Vertical");
        jumping = Input.GetButton("Jump");
    }

   
    

    private void Movement() {
        //Extra gravity
            //Always running, constant force
        rb.AddForce(Vector3.down * Time.deltaTime * 50);
        //In freefall, when not wallrunning, grounded or grappling - Adjustable
        bool isGrappling = grappleGun.GetComponent<GrapplingGun>().grappled;
        if ((!isWallRunning && !isGrappling) && !grounded) 
        {
            rb.AddForce(Vector3.down * Time.deltaTime * extraGravity);
            //flyState.start();
        }

        WallRunTimer();
        //Find actual velocity relative to where player is looking
        Vector2 mag = FindVelRelativeToLook();
        float xMag = mag.x, yMag = mag.y;

        //Counteract sliding and sloppy movement
        CounterMovement(x, y, mag);
               
        //If holding jump && ready to jump, then jump
        if (readyToJump && jumping) Jump();
        
        
        //If speed is larger than maxspeed, cancel out the input so you don't go over max speed
        if (x > 0 && xMag > moveSpeed) x = 0;
        if (x < 0 && xMag < -moveSpeed) x = 0;
        if (y > 0 && yMag > moveSpeed) y = 0;
        if (y < 0 && yMag < -moveSpeed) y = 0;

        //Some multipliers
        float multiplier = 1f;
        
        // Movement in air
        if (!grounded) {
            multiplier = aerialMovementMultiplier;
        }
        

        //Apply forces to move player
        rb.AddForce(orientation.transform.forward * y * moveForce * Time.deltaTime * multiplier);
        rb.AddForce(orientation.transform.right * x * moveForce * Time.deltaTime * multiplier);

        //Pass velocity value to Animator
        anim.SetFloat("PlayerVelocity", rb.velocity.magnitude);
    }

    private void Jump() {
        if (grounded && readyToJump) {
            readyToJump = false;

            anim.SetTrigger("Jump");

            //Add jump forces
            rb.AddForce(Vector2.up * jumpForce * 1.5f);
            rb.AddForce(normalVector * jumpForce * 0.5f);
            
            //If jumping while falling, reset y velocity.
            Vector3 vel = rb.velocity;
            if (rb.velocity.y < 0.5f)
                rb.velocity = new Vector3(vel.x, 0, vel.z);
            else if (rb.velocity.y > 0) 
                rb.velocity = new Vector3(vel.x, vel.y / 2, vel.z);
            
            Invoke(nameof(ResetJump), jumpCooldown);
            FMODUnity.RuntimeManager.PlayOneShot(PlayerJumpEvent, transform.position);      // jump sound
        }
        if (isWallRunning && readyToJump)
        {
            readyToJump = false;

            anim.SetTrigger("Jump");

            //sidwards wallhop

            if (isWallRight)
            {
                rb.AddForce(orientation.up  * wallJumpUpwardForce );
                rb.AddForce(-orientation.right  * wallJumpSideForce );
            }
            if (isWallLeft )
            {
                rb.AddForce(orientation.up  * wallJumpUpwardForce );
                rb.AddForce(orientation.right  * wallJumpSideForce);
            }
           

            Invoke(nameof(ResetJump), jumpCooldown);
            FMODUnity.RuntimeManager.PlayOneShot(PlayerJumpEvent, transform.position);      // jump sound

        }
    }
    
    private void ResetJump() {
        readyToJump = true;
    }
    
    private float desiredX;
    private void Look() {
        if (PauseMenu.GameIsPaused == false){
            float mouseX = Input.GetAxis("Mouse X") * sensitivity * Time.fixedDeltaTime * sensMultiplier;
            float mouseY = Input.GetAxis("Mouse Y") * sensitivity * Time.fixedDeltaTime * sensMultiplier;

            //Find current look rotation
            Vector3 rot = playerCam.transform.localRotation.eulerAngles;
            desiredX = rot.y + mouseX;

            //Rotate, and also make sure we dont over- or under-rotate.
            xRotation -= mouseY;
            xRotation = Mathf.Clamp(xRotation, -90f, 90f);

            //Perform the rotations
            playerCam.transform.localRotation = Quaternion.Euler(xRotation, desiredX, wallRunCameraTilt);
            orientation.transform.localRotation = Quaternion.Euler(0, desiredX, 0);
            
            //While Wallrunning
            //Tilts camera in .5 second
            if (Math.Abs(wallRunCameraTilt) < maxWallRunCameraTilt && isWallRunning && isWallRight)
                wallRunCameraTilt += Time.deltaTime * maxWallRunCameraTilt * cameraRotationSpeed;
            if (Math.Abs(wallRunCameraTilt) < maxWallRunCameraTilt && isWallRunning && isWallLeft)
                wallRunCameraTilt -= Time.deltaTime * maxWallRunCameraTilt * cameraRotationSpeed;

            //Tilts camera back again
            if (wallRunCameraTilt > 0 && !isWallRunning)
                wallRunCameraTilt -= Time.deltaTime * maxWallRunCameraTilt * cameraRotationSpeed;
            if (wallRunCameraTilt < 0 && !isWallRunning)
                wallRunCameraTilt += Time.deltaTime * maxWallRunCameraTilt * cameraRotationSpeed;
        }
    }

    private void CounterMovement(float x, float y, Vector2 mag) {
        if (!grounded || jumping) return;


        //Counter movement
        if (Math.Abs(mag.x) > threshold && Math.Abs(x) < 0.05f || (mag.x < -threshold && x > 0) || (mag.x > threshold && x < 0)) {
            rb.AddForce(moveForce * orientation.transform.right * Time.deltaTime * -mag.x * counterMovement);
        }
        if (Math.Abs(mag.y) > threshold && Math.Abs(y) < 0.05f || (mag.y < -threshold && y > 0) || (mag.y > threshold && y < 0)) {
            rb.AddForce(moveForce * orientation.transform.forward * Time.deltaTime * -mag.y * counterMovement);
        }
        
        //Limit diagonal running. This will also cause a full stop if sliding fast and un-crouching, so not optimal.
        if (Mathf.Sqrt((Mathf.Pow(rb.velocity.x, 2) + Mathf.Pow(rb.velocity.z, 2))) > moveSpeed) {
            float fallspeed = rb.velocity.y;
            Vector3 n = rb.velocity.normalized * moveSpeed;
            rb.velocity = new Vector3(n.x, fallspeed, n.z);
        }
    }

    /// <summary>
    /// Find the velocity relative to where the player is looking
    /// Useful for vectors calculations regarding movement and limiting movement
    /// </summary>
    /// <returns></returns>
    public Vector2 FindVelRelativeToLook() {
        float lookAngle = orientation.transform.eulerAngles.y;
        float moveAngle = Mathf.Atan2(rb.velocity.x, rb.velocity.z) * Mathf.Rad2Deg;

        float u = Mathf.DeltaAngle(lookAngle, moveAngle);
        float v = 90 - u;

        float magnitue = rb.velocity.magnitude;
        float yMag = magnitue * Mathf.Cos(u * Mathf.Deg2Rad);
        float xMag = magnitue * Mathf.Cos(v * Mathf.Deg2Rad);
        
        return new Vector2(xMag, yMag);
    }

    private bool IsFloor(Vector3 v) {
        float angle = Vector3.Angle(Vector3.up, v);
        return angle < maxSlopeAngle;
    }

    private bool cancellingGrounded;
    
    /// <summary>
    /// Handle ground detection
    /// </summary>
    private void OnCollisionStay(Collision other) {
        //Make sure we are only checking for walkable layers
        int layer = other.gameObject.layer;
        if (whatIsGround != (whatIsGround | (1 << layer))) return;

        //Iterate through every collision in a physics update
        for (int i = 0; i < other.contactCount; i++) {
            Vector3 normal = other.contacts[i].normal;
            //FLOOR
            if (IsFloor(normal)) {
                if (!anim.GetBool("Grounded"))
                {
                    //flyState.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
                    FMODUnity.RuntimeManager.PlayOneShot(PlayerLandEvent, transform.position);      // land sound
                }
                grounded = true;
                anim.SetBool("Grounded", true);
                cancellingGrounded = false;
                normalVector = normal;
                CancelInvoke(nameof(StopGrounded));
            }
        }

        //Invoke ground/wall cancel, since we can't check normals with CollisionExit
        float delay = 3f;
        if (!cancellingGrounded) {
            cancellingGrounded = true;
            Invoke(nameof(StopGrounded), Time.deltaTime * delay);
        }
    }

    private void StopGrounded() {
        grounded = false;
        anim.SetBool("Grounded", false);
    }

    //==================================== Wall-Run functions below =============================================================================
    private void CheckForWall() //call in void Update
    {
        isWallRight = Physics.CheckSphere(wallCheckRight.position, .05f, whatIsWall);
        isWallLeft = Physics.CheckSphere(wallCheckLeft.position, .05f, whatIsWall);
        //leave wall run
        if (!isWallLeft && !isWallRight) StopWallRun();
    }
    private void WallRunInput() //call in void Update
    {
        //Wallrun
        if ( Input.GetAxisRaw("Vertical")>0 && isWallRight && !grounded) StartWallrun();
        else if (( Input.GetAxisRaw("Vertical") <=0||grounded) && isWallRight) StopWallRun();
        if ( Input.GetAxisRaw("Vertical") >0 && isWallLeft && !grounded ) StartWallrun();
        else if ((Input.GetAxisRaw("Vertical") <=0||grounded) && isWallLeft) StopWallRun();
    }
    private void StartWallrun()
    {
        isWallRunning = true;
        anim.SetBool("isWallRunning", true);
        rb.useGravity = false;
        if (!isWallTimerOn)
        {
            isWallTimerOn = true;
            timeLeft = maxWallrunTime;
        }
        if (rb.velocity.magnitude <= maxWallSpeed)
        {
            rb.AddForce(orientation.forward * wallrunForce * Time.deltaTime);

            //Make sure char sticks to wall
            if (isWallRight && isWallRunning)
                rb.AddForce(orientation.right * wallrunForce / 5 * Time.deltaTime);
            else if(isWallLeft && isWallRunning)
                rb.AddForce(-orientation.right * wallrunForce / 5 * Time.deltaTime);
        }
    }
    private void StopWallRun()
    {
        isWallRunning = false;
        anim.SetBool("isWallRunning", false);
        isWallTimerOn = false;
        rb.useGravity = true;
        if (!grounded)
        {
            if (isWallRight)
            {
                rb.AddForce(-orientation.right * wallRunExitForce);
                rb.AddForce(orientation.up * (wallRunExitForce / 3));
            }
            else if (isWallLeft)
            {
                rb.AddForce(orientation.right * wallRunExitForce);
                rb.AddForce(orientation.up * (wallRunExitForce / 3) );
            }
        }
        else
        {
            if (isWallRight) rb.AddForce(-orientation.right * wallRunExitForce);
            else if (isWallLeft) rb.AddForce(orientation.right * wallRunExitForce);

        }
        
    }
    void WallRunTimer()
    {
        if (isWallTimerOn)
        {
            if (timeLeft <= 0 && isWallRunning)
            {
                StopWallRun();
                timeLeft = 0;
                isWallTimerOn = false;
            }
            else
            {
                timeLeft -= Time.deltaTime;
            }
        }
        else 
        {
            timeLeft = 0;
        }
    }

    void AudioPlayFootstep()
    {
        //This function is here so it can be called in animation events

        RaycastHit hit;
        Ray footstepRay = new Ray(transform.position, Vector3.down);
        if (Physics.Raycast(footstepRay, out hit, 2f))
        {
            if (hit.collider.tag == "Dirt")
            {
                Debug.Log("Stepping on Grass");
                FMODUnity.RuntimeManager.PlayOneShot(FootstepsGrassEvent, transform.position); // Grass
            }
            else
            {
                Debug.Log("Stepping on Stone");
                FMODUnity.RuntimeManager.PlayOneShot(FootstepsFloorStoneEvent, transform.position); // Stone Floor
            }
        }
        else if (isWallRunning) 
        {
            Debug.Log("Stepping on Wall");
            FMODUnity.RuntimeManager.PlayOneShot(FootstepsWallEvent, transform.position);       // Stone Wall
        }
        else
        {
            Debug.Log("Stepping on Stone");
            FMODUnity.RuntimeManager.PlayOneShot(FootstepsFloorStoneEvent, transform.position); // Stone Floor
        }
        //BUG: SetParameter doesnt work here for some reason

    }

    public void startMusic()
    {
        musicState.start();
    }

    public void stopMusic()
    {
        musicState.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }
}
