﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckGround : MonoBehaviour
{
    public float distance = 2;

    void Update()
    {
        RaycastHit hit;
        Ray footstepRay = new Ray(transform.position, Vector3.down);
        if(Physics.Raycast(footstepRay, out hit, distance)){
            if(hit.collider.tag == "untagged")
                {
                    GameObject myObj = hit.collider.gameObject;
                    Renderer rend = myObj.GetComponent<Renderer>();
                    Material standingOnMaterial = rend.material;
                if (standingOnMaterial.name == "Grass")
                {
                    Debug.Log("Grass");
                }
                if (standingOnMaterial.name == "Ground_Layered")
                {
                    Debug.Log("Ground_Layered");
                }
            }
            }
    }
}