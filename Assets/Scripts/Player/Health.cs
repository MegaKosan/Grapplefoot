﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Health : MonoBehaviour
{
    public Text HealthText;

    public static int health = 5;
    public int numOfHearts;

    public Image[] hearts;
    //public Sprite fullHeart;
    //public Sprite emptyHeart;

    public float restartDelay = 1f;

    private Animator anim;

    [Header("FMOD Sounds")]
    [FMODUnity.EventRef]
    public string PlayerHitEvent = "";

    private void Start()
    {
        anim = GetComponent<Animator>();
        
    }

    private void Update()
    {
        if (health > numOfHearts)
        {
            health = numOfHearts;
        }
        for(int i = 0; i < hearts.Length; i++)
        {

            if (health == 5)
            {
                HealthText.text = "+++++";
            }
            if (health == 4)
            {
                HealthText.text = "++++";
            }
            if (health == 3)
            {
                HealthText.text = "+++";
            }
            if (health == 2)
            {
                HealthText.text = "++";
            }
            if (health == 1)
            {
                HealthText.text = "+";
            }
            if (health <= 0)
            {
                Debug.Log("Game Over");
            }
        }
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "EnemyProjectile")
        {
            anim.SetTrigger("eff_TakeDamage");
            health -= 1;
            FMODUnity.RuntimeManager.PlayOneShot(PlayerHitEvent, transform.position);
        }
        if(other.tag == "Healthpack" && health < 5)
        {
            anim.SetTrigger("eff_PickupHealth");
            health += 1;
            Destroy(other.gameObject);
        }
    }


}
