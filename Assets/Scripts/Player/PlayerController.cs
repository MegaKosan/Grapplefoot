﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] Transform playerCamera = null;
  
    [SerializeField][Range(1, 12)]public float mouseSensitivity = 3.5f;
    [SerializeField] bool cursorIsLocked = true;
 
    [SerializeField] [Range(0, 0.5f)] float mouseSmoothTime = 0.05f;
   
    
    float cameraPitch = 0.0f;
    float cameraYaw = 0.0f;
   


    Vector2 currentMouseDelta = Vector2.zero;
    Vector2 currentMouseDeltaVelocity = Vector2.zero;

    CharacterController controller = null;
    [SerializeField] [Range(5, 15)] public float walkSpeed;
    [SerializeField] [Range(5, 30)] public float runSpeed;
    [SerializeField] [Range(2, 10)] public float airSpeed;
    [SerializeField] float gravity = -13f;
    [SerializeField] float jumpHeight = 3f;
    float movementSpeed = 5f;
    public float inertiaTime = 0.5f;
    float velocityY = 0f;
    public KeyCode runkey;
    Vector3 velocity = Vector3.zero;
    Vector2 currentDirection = Vector2.zero;
    Vector2 currentDirectionVelocity = Vector2.zero;
    Vector2 targetDirection=Vector2.zero;
    [SerializeField] [Range(0, 1)] float moveSmoothTime = .5f;
    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
        if (cursorIsLocked) 
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        UpdateMouseLook();
        UpdateMovement2();
    }

    void UpdateMouseLook() 
    {
        Vector2 targetMouseDelta = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
        currentMouseDelta = Vector2.SmoothDamp(currentMouseDelta, targetMouseDelta, ref currentMouseDeltaVelocity, mouseSmoothTime);
        cameraPitch -= currentMouseDelta.y * mouseSensitivity;
        cameraPitch = Mathf.Clamp(cameraPitch, -90, 90);
        cameraYaw += currentMouseDelta.x * mouseSensitivity;
        cameraYaw = Mathf.Clamp(cameraYaw, -90, 90);
        Vector3 cameraAngle = new Vector3(cameraPitch,cameraYaw,0.0f);

        playerCamera.localEulerAngles = Vector3.right * cameraPitch;
        
        transform.Rotate(Vector3.up * currentMouseDelta.x * mouseSensitivity);
       /* if (controller.isGrounded)
        {
            playerCamera.localEulerAngles = Vector3.right * cameraPitch;
            transform.Rotate(Vector3.up * currentMouseDelta.x * mouseSensitivity);
        }
        else 
        { 
           
             playerCamera.localEulerAngles = cameraAngle;
        }
       */
    }

    void UpdateMovement() 
    {
        float horizonatInput = Input.GetAxisRaw("Horizontal");//moves sidewise
        float verticalInput = Input.GetAxisRaw("Vertical");//moves forward
        Vector2 targetDirection = new Vector2(horizonatInput,verticalInput);
        targetDirection.Normalize();



        if (controller.isGrounded)
        {

            velocityY = -2.0f;
            if (Input.GetKey(runkey))
            {
                movementSpeed = Mathf.Lerp(movementSpeed, runSpeed, inertiaTime);
            }
            else
            {
                movementSpeed = Mathf.Lerp(movementSpeed, walkSpeed, inertiaTime);
            }

            if (Input.GetButton("Jump"))
            {
                velocityY = Mathf.Sqrt(jumpHeight * -2 * gravity);
                
            }

        }
        else 
        {
            velocityY += gravity * Time.deltaTime;

            currentDirection = Vector2.SmoothDamp(currentDirection, targetDirection, ref currentDirectionVelocity, moveSmoothTime);
            velocity = (transform.forward * currentDirection.y + transform.right * currentDirection.x) * movementSpeed + Vector3.up * velocityY;
            controller.Move(velocity * Time.deltaTime);
        }
        
        
        
    
    }

    void UpdateMovement2()
    {

       
        if (controller.isGrounded)
        {
            float horizonatInput = Input.GetAxisRaw("Horizontal");//moves sidewise
            float verticalInput = Input.GetAxisRaw("Vertical");//moves forward
            targetDirection = new Vector2(horizonatInput, verticalInput);

            velocityY = -2.0f;
            if (Input.GetKey(runkey))
            {
                movementSpeed = Mathf.Lerp(movementSpeed, runSpeed, inertiaTime);
            }
            else
            {
                movementSpeed = Mathf.Lerp(movementSpeed, walkSpeed, inertiaTime);
            }
            currentDirection = Vector2.SmoothDamp(currentDirection, targetDirection, ref currentDirectionVelocity, moveSmoothTime);
            velocity = (transform.forward * currentDirection.y + transform.right * currentDirection.x) * movementSpeed + Vector3.up * velocityY;
            controller.Move(velocity * Time.deltaTime);

            if (Input.GetButton("Jump"))
            {
                Jump(verticalInput,currentDirection);

            }

        }
        else
        {
            velocityY += gravity * Time.deltaTime;
           // movementSpeed = Mathf.Lerp(movementSpeed, airSpeed, inertiaTime);
            float horizonatInput = Input.GetAxisRaw("Horizontal");//moves sidewise
            currentDirection = Vector2.SmoothDamp(currentDirection, targetDirection, ref currentDirectionVelocity, moveSmoothTime);
            velocity = (transform.forward * currentDirection.y  + transform.right * currentDirection.x) * movementSpeed + Vector3.up * velocityY;
            controller.Move(velocity * Time.deltaTime);
        }

    }

    void Jump(float forwardSpeed, Vector2 direction) 
    {
        velocityY = Mathf.Sqrt(jumpHeight * -2 * gravity);
        //currentDirection = Vector2.SmoothDamp(currentDirection, targetDirection, ref currentDirectionVelocity, moveSmoothTime);
        //velocity = (transform.forward * forwardSpeed * movementSpeed + transform.right * currentDirection.x * movementSpeed)  + Vector3.up * velocityY;//1st experiment
        //velocity = new Vector3(currentDirection.x*movementSpeed,velocityY, currentDirection.y * forwardSpeed * movementSpeed);//2nd experiment
        velocity = new Vector3(direction.x *movementSpeed, velocityY, direction.y * forwardSpeed * movementSpeed);//3rd experiment with vector parameter
        controller.Move(velocity * Time.deltaTime);
    }
}
