﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttack : MonoBehaviour
{
    /*------------------Audio-Start---------------------*/
    [FMODUnity.EventRef]
    public string KickChargeEvent = "";
    FMOD.Studio.EventInstance chargeState;

    [FMODUnity.EventRef]
    public string KickHitEvent = "";

    [FMODUnity.EventRef]
    public string KickMissEvent = "";


    /*------------------Audio-End-----------------------*/



    public Camera cam;
    public Transform PlayerHead;
    Animator anim;
    public GameObject dustParticles;
    //public float SecondsInBetweenKickAnims = 0.75f;

    float attackRange = 3f;

    public float SpherecastRadius = 0.5f;
    public float SpherecastRadiusProjectiles = 1;


    public Transform enemy;
    public LayerMask whatIsEnemy;
    public bool enemyInAttackRange;

    public Rigidbody rb;

    public float WallPushbackForce = -500;


    float angle;
    void Start()
    {
        
        anim = GetComponent<Animator>();

        chargeState = FMODUnity.RuntimeManager.CreateInstance(KickChargeEvent);
        
    }

    private void Awake()
    {
        
        enemy = GameObject.Find("Enemy").transform;
    }
    void Update()
    {
        enemyInAttackRange = Physics.CheckSphere(transform.position, attackRange, whatIsEnemy);


        if (Input.GetButtonDown("Kick"))
        {
            ChargeAttack();
        }
        /*if (Input.GetButton("Kick"))
        {
            ChargeAttack();
        }*/
        if (Input.GetButtonUp("Kick"))
        {
            DoAttack();
        }
        /*if (enemyInAttackRange == true)
        {
            DoAttack();
        }*/


    }
    private void ChargeAttack()
    {
        anim.SetBool("KickCharge", true);
        //StartCoroutine(Timer());
        chargeState.start();          // charge


    }
    /*IEnumerator Timer()
    {
        yield return new WaitForSeconds(SecondsInBetweenKickAnims);
        DoAttack();
    }*/
    private void DoAttack()
    {
        
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        anim.SetBool("KickCharge", false);
        //TODO: Stop Charge Event here
        chargeState.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);


        if (Physics.SphereCast(ray, SpherecastRadiusProjectiles, out hit, attackRange*1.5f))
        {
             if (hit.collider.tag == "EnemyProjectile")
            {
                hit.collider.gameObject.GetComponent<ProjectileDestroyer>().OnKick();
                FMODUnity.RuntimeManager.PlayOneShot(KickHitEvent, transform.position);     // hit

            }
            
        }
        else
        {
            FMODUnity.RuntimeManager.PlayOneShot(KickMissEvent, transform.position);        // miss
        }

        if (Physics.Raycast(ray, out hit, attackRange))
            {

            if (hit.collider.tag == "Enemy" )
            {
                hit.collider.gameObject.GetComponent<EnemyAI>().TakeDamage(200);
                FMODUnity.RuntimeManager.PlayOneShot(KickHitEvent, transform.position);     // hit
            }

            if (hit.collider.tag == "Untagged")
                 {
                    Debug.Log("BONK");
                    rb.AddForce(PlayerHead.transform.forward * WallPushbackForce);
                    Instantiate(dustParticles, hit.point, PlayerHead.transform.rotation);
                    FMODUnity.RuntimeManager.PlayOneShot(KickHitEvent, transform.position); // hit

            }
            if (hit.collider.tag == "Gem")
            {
                hit.collider.gameObject.GetComponent<GemScript>().OnKick();
                FMODUnity.RuntimeManager.PlayOneShot(KickMissEvent, transform.position);    // miss
            }

        }
        else
        {
            FMODUnity.RuntimeManager.PlayOneShot(KickMissEvent, transform.position);        // miss
        }


    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, attackRange);
    }
}
