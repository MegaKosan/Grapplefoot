﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawner : MonoBehaviour
{
    public Rigidbody rb;
    public GrapplingGun gun;
    private Animator anim;

    [FMODUnity.EventRef]
    public string respawnSound = "";

    Transform GetNearestPoint()
    {
        Transform nearestPoint = null;
        float minDist = Mathf.Infinity;
        Vector3 currentPosition = transform.position;
        GameObject[] allPoints = GameObject.FindGameObjectsWithTag("SpawnPoint");

        foreach (GameObject point in allPoints)
        {
            float dist = Vector3.Distance(point.transform.position, currentPosition);
            if (dist < minDist)
            {
                nearestPoint = point.transform;
                minDist = dist;
            }

        }
        
        return nearestPoint;
    }

    private void Start()
    {
        anim = GetComponent<Animator>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Boundary") 
        {
            gun.StopGrapple();
            transform.position = GetNearestPoint().position;
            transform.rotation = GetNearestPoint().rotation;
            rb.velocity = Vector3.zero;

            anim.SetTrigger("eff_Respawning");
            FMODUnity.RuntimeManager.PlayOneShot(respawnSound);
        }
    }
}
