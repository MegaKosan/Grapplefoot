﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI : MonoBehaviour
{
    //Assignables
    [Header("Assignables")]
    public Transform player;
    public Transform rotationTransform;
    public Transform bulletSpawnPoint;
    public GameObject turretTop;
    public Transform muzzleDevice;
    public GameObject destroyedProp;
    public GameObject explosionParticle;


    //Layer masks
    [Header("Layer Masks")]
    public LayerMask whatIsGround;
    public LayerMask whatIsPlayer;

    //Values
    [Header("Values")]
    public float health;
    public float ProjectileSpeed = 16f;
    //public float ProjectileUpdraft = 3f;

    //attack
    [Header("Attack")]
    public float timeBetweenAttacks;
    bool alreadyAttacked;
    public GameObject projectile;

    //States
    [Header("States")]
    public float sightRange;
    public float attackRange;
    public bool playerInSightRange;
    public bool playerInAttackRange;
    private bool isDead;
    private bool isAwake;
    private bool propReplaced;
    private bool targetAcquired;

    //Sound Sources
    [Header("Audio Sources")]
    public AudioSource audioShoot;
    public AudioSource audioWakeUp;
    public AudioSource audioChargeUp;

    //Animator
    private Animator anim;


    
    private void Awake()
    {
        isDead = false;
        propReplaced = false;
        player = GameObject.Find("Player").transform;
        anim = GetComponent<Animator>();
        alreadyAttacked = false;
    }
    private void Update()
    {
        if (!isDead)
        {
            RadarScan();
            if (!playerInSightRange) 
            {
                if (isAwake)
                {
                    isAwake = false;
                    anim.SetBool("Awake", false);
                    //Play sleep sound here
                }
            }
            if (playerInSightRange && playerInAttackRange)
            {
                AcquireTarget();
                if (targetAcquired) ChargeUpGun();
            }
        }
        else 
        {     
            if (!propReplaced) PerformDeath();
        }

    }
    private void AttackPlayer()
    {
        if (!isDead) { 
        anim.SetBool("Charge", false);
        audioShoot.Play();
        Rigidbody rb = Instantiate(projectile, bulletSpawnPoint.position, Quaternion.identity).GetComponent<Rigidbody>();  
        rb.AddForce(rotationTransform.forward * ProjectileSpeed, ForceMode.Impulse);
        }     
    }
    private void ResetAttack()
    {
        alreadyAttacked = false;
    }
    public void TakeDamage(int damage)
    {
        if (!isDead)
        {
            health -= damage;

            if (health <= 0)
            {
              
                isDead = true;
            }
        }
    }
    IEnumerator GetReady()
    {
        yield return new WaitForSeconds(1.6f);
        if (!isAwake)
        {
            isAwake = true;
            audioWakeUp.Play();
            anim.SetBool("Awake", true);
        }
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackRange);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, sightRange);
    }

    void PerformDeath()
    {
        anim.SetBool("isDead", true);
        Instantiate(explosionParticle,rotationTransform.position, Quaternion.identity);
        //Instantiate(destroyedProp,turretTop.transform.position,turretTop.transform.rotation);
        turretTop.SetActive(false);
        propReplaced = true;
        // change the Physics layer and tag to avoid enemy related events from other scripts
        //transform.gameObject.tag = "Untagged";
        transform.gameObject.layer = 0;//default layer
        transform.gameObject.name = "DestroyedEnemy";
        rotationTransform.gameObject.GetComponent<EnemyTargeting>().enabled = false;
        gameObject.GetComponent<EnemyAI>().enabled = false;


    }
   

    void AcquireTarget() 
    {
        rotationTransform.LookAt(player);
      
        RaycastHit hit;
        
        if (Physics.Raycast(rotationTransform.position, rotationTransform.forward, out hit))
        {
            if (hit.collider != null)
            {
                if (hit.collider.tag == "Player")
                {
                    
                    if (!isAwake)
                    {
                        StartCoroutine(GetReady()); 
                    }
                    else 
                    {
                        targetAcquired = true;
                    }
                }
                else 
                {  
                    targetAcquired = false;
                    
                }

            }

          
        }
    }
    void ChargeUpGun() 
    {
        if (!alreadyAttacked)
        {
            alreadyAttacked = true;
            anim.SetBool("Charge",true);
            Invoke(nameof(ResetAttack), timeBetweenAttacks);
            audioChargeUp.Play();
        }
    }
    void RadarScan() 
    {
        playerInSightRange = Physics.CheckSphere(transform.position, sightRange, whatIsPlayer);
        playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, whatIsPlayer);
        
    }
    
}
