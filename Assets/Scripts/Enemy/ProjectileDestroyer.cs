﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileDestroyer : MonoBehaviour
{
    private void Update()
    {
        Destroy(gameObject, 4f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Melee" || other.gameObject.tag == "Untagged" || other.gameObject.tag == "Boundary" )
        {
            Destroy(gameObject);
        }
    }

    public void OnKick()
    {
        GetComponent<Rigidbody>().velocity *= -2;
    }
}
