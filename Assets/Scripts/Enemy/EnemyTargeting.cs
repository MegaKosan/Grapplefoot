﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTargeting : MonoBehaviour
{

    public readonly static HashSet<EnemyTargeting> Pool = new HashSet<EnemyTargeting>();

    private void OnEnable()
    {
        EnemyTargeting.Pool.Add(this);
    }

    private void OnDisable()
    {
        EnemyTargeting.Pool.Remove(this);
    }


    public static EnemyTargeting FindClosestEnemy(Vector3 pos)
    {
        EnemyTargeting result = null;
        float dist = float.PositiveInfinity;
        var e = EnemyTargeting.Pool.GetEnumerator();
        while (e.MoveNext())
        {
            float d = (e.Current.transform.position - pos).sqrMagnitude;
            if (d < dist)
            {
                result = e.Current;
                dist = d;
            }
        }
        return result;
    }

}


